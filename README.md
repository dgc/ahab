Alexa/Home Assistant Bridge (AHAB)
==================================

AHAB is an Alexa Skills Kit (ASK) deployment providing a Home Assistant bridge
skill. It automates skill setup and AWS Lambda deployment, substantially
simplifying the task of bridging Alexa voice controls to your HA installation.
It’s intended to replace many of the more unfamiliar steps described at [Amazon
Alexa Smart Home
Skill](https://www.home-assistant.io/integrations/alexa.smart_home/), though
there is still much there that’s relevant.

To deploy AHAB, you need a Unixy environment (Linux, FreeBSD, MacOS, WSL2, …)
and the following additional tools:

-   [AWS CLI v2](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)

-   [ASK CLI](https://developer.amazon.com/en-US/docs/alexa/smapi/quick-start-alexa-skills-kit-command-line-interface.html)

-   [jq](https://stedolan.github.io/jq/)
 

Two-step deployment
-------------------

1. Prepare the skill and deploy it. This also deploys the Lambda code. To do
this you will need both ASK CLI and AWS CLI configured in advance.

```
$ ask deploy
```

2. Update the skill with account linking information. This script handles
everything for you, but it assumes that the default AWS CLI profile is where you
will deploy your Lambda code. Make sure that’s true, e.g. by settings
AWS_PROFILE or similar in your environment.

```
$ ./account-link.sh your-internet-accessible-HA-address
```

For example:

```
$ ./account-link.sh ha.example.com
```

This script runs several commands for you to configure skill linking. It needs
your HA address to configure the skill linking and to tell the Lambda
installation where to forward Alexa directives. It only supports HTTPS
endpoints, because you really should not put plaintext HA on the internet.

 
