#!/bin/sh
# vim: ts=4 et

usage () {
    p=$(basename "$0")
    echo "usage: $p ha-url [skill-id]"
}

case "$#/$1" in
    */-h|*/--help)
        usage
        exit 0
        ;;

    1/*|2/*)
        haurl=https://${1##http*://}
        skid=$2
        ;;

    *)
        usage >&2
        exit 2
        ;;
esac

jq -h >/dev/null || {
    echo >&2 "error: this program requires jq"
    exit 1
}
ask -V >/dev/null || {
    echo >&2 "error: this program requires the Alexa Skill Kit CLI (ask)"
    exit 1
}
aws help >/dev/null || {
    echo >&2 "error: this program requires aws-cli v2"
    exit 1
}


secret=$(openssl rand -base64 32)
if [ -z "$skid" ]; then
    echo 'Querying AHAB skill ID...'
    skid=$(
        ask smapi list-skills-for-vendor |
        jq -r '.skills[] | select(.nameByLocale["en-US"] == "AHAB") | .skillId'
    )
    echo "... $skid"
fi

cd $(dirname "$0")

# Get function arn to populate environment variables
farn=$(jq -r .manifest.apis.smartHome.endpoint.uri <skill-package/skill.json)
fname=$(echo $farn | cut -d: -f7)
fregion=$(echo $farn | cut -d: -f4)

# Update function configuration
echo "Updating function configuration"
aws --region $fregion lambda update-function-configuration \
    --function-name $fname \
    --environment "Variables={HA_URL=$haurl,TLS_VERIFY=1}" \
    > /dev/null || echo "failed"

# Prepare linking request
request=$(
    cat linking-request.json |
    sed -e "s!{secret}!$secret!" \
        -e "s!{habase}!$haurl!"
)
echo "Updating skill linking information"
ask smapi update-account-linking-info \
    --skill-id $skid \
    --account-linking-request "$request"

echo
echo 'You may also wish to enable Alexa Events. Open this URL to do so:'
echo "https://developer.amazon.com/alexa/console/ask/build/permissions-v2/$skid/development/en_US"
