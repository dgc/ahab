# vim: ts=4 et

'''
This code is all written from scratch by dgc.ha@c13.us, but it
is heavily based on eyeball reference to
https://gist.github.com/matt2005/744b5ef548cc13d88d0569eea65f5e5b

Which bears the following copyright notice:

Copyright 2019 Jason Hu <awaregit at gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
'''

import os
import json
import urllib3


class AlexaError(RuntimeError):
    '''Alexa runtime message/directive error'''


def abend(exc=None, cls=None, full=False):
    if not exc:
        import sys
        exc = sys.exc_info()[1]
    tb = exc.__traceback__
    condition = str(exc)
    cls = cls or exc.__class__
    with open(tb.tb_frame.f_code.co_filename) as fp:
        for n, l in enumerate(fp, start=1):
            s = l.strip()
            if s.startswith('#!'):
                condition = s[2:].strip()
                continue
            if n == tb.tb_lineno:
                if full:
                    raise cls(condition) from exc
                else:
                    raise cls(condition) from None
    # else
    raise exc


# When placing your HA on the Internet, one way to protect it is
# with a proxying HTTP server. You can implement header-based access
# controls there. If GATEWAY_TOKEN is set in your Lambda environment,
# it's assumed to look like X-Header-Name:tokenvalue. We'll add that
# header name/value pair to requests to your HA.
GatewayToken = os.environ.get('GATEWAY_TOKEN')

# Get HA instance address
HaUrl = os.environ['HA_URL']

# Get verification needs
if (bool(os.environ.get('TLS_VERIFY', True))
    and # bleh, compatibility
    not bool(os.environ.get('NOT_VERIFY_SSL', False))
):
    verify = 'CERT_REQUIRED'
else:
    verify = 'CERT_NONE'
Pool = urllib3.PoolManager(cert_reqs=verify, timeout=5.0)


def ahab(event, context):
    print('Alexa request:')
    print(json.dumps(event))

    haUri = 'https://home.c13.us/'
    haUri = haUri.strip('/')
    if '://' not in haUri:
        haUri = f'https://{haUri}'

    try:
        #! not a valid Alexa directive
        directive = event['directive']

        #! no version in directive
        version = int(directive['header']['payloadVersion'])

        #! only directive schema version 3 is supported
        assert version == 3

        #! error getting directive scope
        endpoint = directive.get('endpoint', {})
        payload = directive.get('payload', {})

    except:
        abend(cls=AlexaError)


    # Get token...
    token = None

    # For AuthN, token is in directive.payload.grantee.token
    if not token:
        try:
            token = payload['grantee']['token']
        except:
            pass

    # For Discovery, token is in directive.payload.scope.token
    if not token:
        try:
            token = payload['scope']['token']
        except:
            pass

    # For ReportState and controls, token is in directive.endpoint.scope.token
    if not token:
        try:
            token = endpoint['scope']['token']
        except:
            pass

    if not token:
        raise AlexaError('no access token provided')

    headers = {
        'Authorization': 'Bearer {}'.format(token),
        'Content-Type': 'application/json',
    }
    if GatewayToken:
        headers.update([gatewaytoken.split(':', 1)])


    # urllib3 is tragic, but that's what AWS provides in the standard
    # lambda runtime, so it's that or http.client :(
    rsp = Pool.request(
        'POST', f'{HaUrl}/api/alexa/smart_home',
        headers=headers,
        body=json.dumps(event)
    )

    data = json.loads(rsp.data.decode())
    print('HA response:')
    print(json.dumps(data))

    if rsp.status >= 500:
        return {
            'event': {
                'payload': {
                    'type': 'INTERNAL_ERROR',
                    'message': f'server error: {data}',
                }
            }
        }

    if rsp.status in (401, 403):
        return {
            'event': {
                'payload': {
                    'type': 'INVALID_AUTHORIZATION_CREDENTIAL',
                    'message': data,
                }
            }
        }

    if rsp.status >= 400:
        return {
            'event': {
                'payload': {
                    'type': 'INTERNAL_ERROR',
                    'message': f'content unavailable: {data}',
                }
            }
        }

    return data
